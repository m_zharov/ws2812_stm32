# README #

This README is very basic, hastily cobbled together for anyone that happens across this code.

### WS2812 LED Controller ###

* Quick summary

Warning: It's currently a hastily cobbled together patchwork.

How it works:
-------------
Each LED is programmed with g, r, b (255,255,255)

Each LED requires 3 bits for each 1 or 0 (see PNG in top level).

To do this most efficiently from a memory utilization point, I'm storing 3 x uint8_t per LED-- this allows me to control 8 GPIO ports in parallel... but this means that for an LED to be changed, a single bit needs to be changed. There are ups and downs for each method I could have used- I'd say this is mostly optimised for text display.

More Detail:
-------------
The first uint8_t is always 0xFF.

The second defines off or on for the parallel set of LEDs.

The third is always 0x00.


So, 3 variables laid out vertically is:

~~~
1 1 0 = LED 1 String 1 = ON
1 0 0 = LED 1 String 2 = OFF
1 0 0 = LED 1 String 3 = OFF
1 0 0 = LED 1 String 4 = OFF
1 0 0 = LED 1 String 5 = OFF
1 0 0 = LED 1 String 6 = OFF
1 0 0 = LED 1 String 7 = OFF
~~~

Which means that my current single serial chain is really a waste of memory.

But-- If I one day use 8x5 text characters to display text on 8 parallel strings it'll be awesomely fast and easy to update.



====================================

The rest of this readme is TODO....


* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact