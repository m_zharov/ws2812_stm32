#include <stm32f4xx_gpio.h>
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_tim.h> // Timer
#include <stm32f4xx_usart.h>
#include <stm32f4xx_spi.h>
#include <stm32f4xx_dma.h>
#include <misc.h> // NVIC
#include <math.h>
#include <tgmath.h>
#include <string.h>
#include <cstdlib>
#include <stdio.h> // Debug messages

#include "Settings.h"
#include "Bluetooth.h"	// Bluetooth Stuff

/////////////
// Defines //
/////////////
#define SPI_LED_BLANK 0x00
#define SPI_LED_0H 0xC0
#define SPI_LED_1H 0xF0

#define UART_LED_BLANK 0x00
#define UART_LED_0H 0xC0
#define UART_LED_1H 0xF8

#define NUMLEDS 401
#define NUMLOBES 6

/////////////////////////
// Static Const Memory //
/////////////////////////
static const int LEDZero = 0x0000FF;

/////////////////
// Global Vars //
/////////////////
// Settings vars
Settings settings;
char cNewLine[3] = "\r\n";
char cCommandAccepted[20] = "OK!\r\n";
uint8_t cCommand[4] = "";
BT bt;

//????????????????//
// Struct to hold green, red and blue data for a frame
// - This struct gets the planned colours loaded in before being sent to the rgb struct.
// - Any modification to a frame should be implemented in here and loaded across.
typedef struct
{
	uint8_t green;
	uint8_t red;
	uint8_t blue;
} rgbCalc;
//????????????????//

typedef struct
{
	// Each encoded bit requires three bits to be stored
	//   '0' = 100  ~ 0.35us high, 0.80us low
	//   '1' = 110  ~ 0.70us high, 0.65us low
	// As the DMA controller can control an entire port, 8 pins will be used in parallel allowing for 8 streams.
	// This means that in the 24 bits stored per colour, 3 will be used per port ~ green[0]{0}, green[1]{0}, green[2]{0}
	
	uint8_t green[24];	// 3 bits per digit, 8 digits per colour = 24
	uint8_t red[24];
	uint8_t blue[24];
	
	// Zero ALL leds (Also resets first and third bits for signal)
	bool setZero()
	{
		for (int i = 0; i < 72; i = i + 3)
		{
			memcpy(&green[i], &LEDZero, 3);
		}
	}
	// Zero specific LED in stream
	bool setZero(uint8_t stream)
	{
		for (int i = 0; i < 72; i = i + 3)
		{
			green[i] |= (0x01 << stream);		// High
			green[i + 1] &= ~(0x01 << stream);	// Low
			green[i + 2] &= ~(0x01 << stream);  // Low
		}
	}
	// Fade specific LEDs in stream by amount
	bool dim(uint8_t stream, uint8_t amount)
	{
		uint8_t tempGreen = 0, tempRed = 0, tempBlue = 0;

		// Copy Old Value to char
		for (int i = 0; i < 8; ++i)
		{
			tempGreen |= ((green[1 + (i * 3)] >> stream) & 0x01) << i;
		}
		
		// Copy Old Value to char
		for (int i = 0; i < 8; ++i)
		{
			tempRed |= ((red[1 + (i * 3)] >> stream) & 0x01) << i;
		}

		// Copy Old Value to char
		for (int i = 0; i < 8; ++i)
		{
			tempBlue |= ((blue[1 + (i * 3)] >> stream) & 0x01) << i;
		}

		// subtract 'amount' from LED
		tempGreen -= amount;
		tempRed -= amount;
		tempBlue -= amount;

		setLED(stream, tempRed, tempGreen, tempBlue);
	}

	// Stream = 0 - 7, 
	bool setLED(uint8_t stream, uint8_t inRed, uint8_t inGreen, uint8_t inBlue)
	{
		// Check for errors
		if (stream > 8)
		{
			return false;
		}
		
		///////////////
		// VARIABLES //
		///////////////
		// Temporary colour array
		uint8_t temp[8];
		
		///////////
		// Green //
		///////////
		// Split colour in to easier to manage array
		for (int i = 7; i >= 0; i--) {
			temp[i] = 0x01 & inGreen;
			inGreen = inGreen >> 1; 
		}
		// Load values in to appropriate LED bits
		for (int i = 0; i < 8; ++i)
		{
			if (temp[i] == 1)
			{
				green[1 + (i * 3)] |= (1 << stream);
			}
			else
			{
				green[1 + (i * 3)] &= ~(1 << stream);
			}
		}
		
		/////////
		// Red //
		/////////
		// Split colour in to easier to manage array
		for (int i = 7; i >= 0; i--) {
			temp[i] = 0x01 & inRed;
			inRed = inRed >> 1; 
		}
		// Load values in to appropriate LED bits
		for (int i = 0; i < 8; ++i)
		{
			if (temp[i] == 1)
			{
				red[1 + (i * 3)] |= (1 << stream);
			}
			else
			{
				red[1 + (i * 3)] &= ~(1 << stream);
			}
		}
		
		//////////
		// Blue //
		//////////
		// Split colour in to easier to manage array
		for (int i = 7; i >= 0; i--) {
			temp[i] = 0x01 & inBlue;
			inBlue = inBlue >> 1; 
		}
		// Load values in to appropriate LED bits
		for (int i = 0; i < 8; ++i)
		{
			if (temp[i] == 1)
			{
				blue[1 + (i * 3)] |= (1 << stream);
			}
			else
			{
				blue[1 + (i * 3)] &= ~(1 << stream);
			}
		}
		
		return true;
	}
} rgb;

typedef struct
{
	// This struct exists to hold an easy collection of LED data, along with guaranteed blank space at the start AND end of transmission, keeps things clean.
	uint8_t blankStart[24] = { 0 };	// Blank array to guarantee long nothingness
	rgb rgbLED[NUMLEDS];
	uint8_t blankEnd[150] = { 0 };
} rgbStrip;


typedef struct {
	double r;       // percent
	double g;       // percent
	double b;       // percent
} rgbx;

typedef struct {
	double h;       // angle in degrees
	double s;       // percent
	double v;       // percent
} hsv;

static hsv   rgb2hsv(rgbx in);
static rgbx   hsv2rgb(hsv in);

hsv rgb2hsv(rgbx in)
{
	hsv         out;
	double      min, max, delta;

	min = in.r < in.g ? in.r : in.g;
	min = min  < in.b ? min : in.b;

	max = in.r > in.g ? in.r : in.g;
	max = max  > in.b ? max : in.b;

	out.v = max;                                // v
	delta = max - min;
	if (delta < 0.00001)
	{
		out.s = 0;
		out.h = 0; // undefined, maybe nan?
		return out;
	}
	if (max > 0.0) { // NOTE: if Max is == 0, this divide would cause a crash
		out.s = (delta / max);                  // s
	}
	else {
		// if max is 0, then r = g = b = 0              
		// s = 0, v is undefined
		out.s = 0.0;
		out.h = NAN;                            // its now undefined
		return out;
	}
	if (in.r >= max)                           // > is bogus, just keeps compilor happy
		out.h = (in.g - in.b) / delta;        // between yellow & magenta
	else
		if (in.g >= max)
			out.h = 2.0 + (in.b - in.r) / delta;  // between cyan & yellow
		else
			out.h = 4.0 + (in.r - in.g) / delta;  // between magenta & cyan

	out.h *= 60.0;                              // degrees

	if (out.h < 0.0)
		out.h += 360.0;

	return out;
}


rgbx hsv2rgb(hsv in)
{
	double      hh, p, q, t, ff;
	long        i;
	rgbx         out;

	if (in.s <= 0.0) {       // < is bogus, just shuts up warnings
		out.r = in.v;
		out.g = in.v;
		out.b = in.v;
		return out;
	}
	hh = in.h;
	if (hh >= 360.0) hh = 0.0;
	hh /= 60.0;
	i = (long)hh;
	ff = hh - i;
	p = in.v * (1.0 - in.s);
	q = in.v * (1.0 - (in.s * ff));
	t = in.v * (1.0 - (in.s * (1.0 - ff)));

	switch (i) {
	case 0:
		out.r = in.v;
		out.g = t;
		out.b = p;
		break;
	case 1:
		out.r = q;
		out.g = in.v;
		out.b = p;
		break;
	case 2:
		out.r = p;
		out.g = in.v;
		out.b = t;
		break;

	case 3:
		out.r = p;
		out.g = q;
		out.b = in.v;
		break;
	case 4:
		out.r = t;
		out.g = p;
		out.b = in.v;
		break;
	case 5:
	default:
		out.r = in.v;
		out.g = p;
		out.b = q;
		break;
	}
	return out;
}

/////////////////////////
// Function Prototypes //
/////////////////////////
void InitClockOutput();
void initGPIO();
void initGPIODMA();
void initGPIODMATimer();
void startGPIODMA(uint32_t address);
void InitMainTimer(uint16_t dTime);
int mod(int x, int m);


//////////////////////
// Global Variables //
//////////////////////
rgbStrip rgbStrips;
bool isDMAOccupied = false;

int main()
{
	///////////////////////////////
	// Set Initial Core Settings //
	///////////////////////////////
	settings.setUpdateMillis(30);
	settings.staticBright = false;
	settings.LEDMode = 3;
	settings.colour_g = 255;
	settings.colour_r = 255;
	settings.colour_b = 255;
	settings.intensity = 100;
	settings.HSVStepSize = 500000;
	settings.BrightnessStepSize = 0.04;

	// Initialise LED array to have appropriate starting values
	//  - All off
	for (int i = 0; i < NUMLEDS; ++i)
	{
		rgbStrips.rgbLED[i].setZero();
	}

	bt.initialise();

	// Init IO
	//InitClockOutput();
	// GPIO DMA Method
	initGPIO();
	initGPIODMA();
	initGPIODMATimer();

	//????????????????//
	// Variables for storing pattern
	rgbCalc rgbValues[NUMLEDS];		//? This variable contains the colour intensity data described waaay above
	// Pattern 1
	int lobe[NUMLOBES];				//? This array of integers contains the centre positions of each 'specular lobe'
	// Pattern 2
	int x = 0, y = 23;

	// Random Numbers
	double random = 0, oldRandom = 0;
	double brightness = 0;
	bool rising = true;

	//char btSend[] = "AT+NAMERainbowLights";	// SET NAME
	//char btSend[] = "AT+PIN7246";	// SET PIN
	//bt.BT_Send(btSend, strlen(btSend));
	
	// Init the one clock to rule them all
	InitMainTimer(1); // Interrupt every 1ms

	// Run main loop
	for (;;)
	{
		bt.ParseString();
		
		/////////////////////////
		// Update Pattern Data //
		/////////////////////////
		// This method has a fatal flaw, overlapping lobes will not be drawn, only the closest (due to IF and ELSE IF)
		// An additional problem is that if lobes are close tails will be overwritten.
		// - A solution would be to set rgbValues[i].green = rgbValues[i].green + 150;
		//    - (same for green and blue)
		//    - (the current method is easier to type but more absolute -- rgbValues[i] = { 150, 0, 0 };

		if (settings.calculationReady == false)
		{
			
			//? Turn off ALL leds - Start with a clean slate by filling the entire array with 0s
			for (int i = 0; i < NUMLEDS; ++i)
			{
				rgbValues[i] = { 0 };
			}

			// Set new brightness level.
			if (settings.staticBright)
			{
				brightness = 1.0;
			}
			else
			{
				if (rising)
				{
					brightness = brightness + settings.BrightnessStepSize;

					if (brightness >= 1)
					{
						rising = false;
						brightness = 1;
					}

				}
				else
				{
					brightness = brightness - settings.BrightnessStepSize;
					if (brightness <= 0.1)
					{
						rising = true;
						brightness = 0.1;
					}
				}
			}


			if (settings.LEDMode == 0)
			{
				///////////////
				// PATTERN 0 //
				///////////////
				// TODO : use HUE to set color with sine function
				//        use different sine function with intensity
				double degrees, value;
				double length, speed;
				double j;
		
				for (int i = 0; i < NUMLEDS; ++i)
				{
					//degrees = float(x + ((255 / NUMLEDS) * i)) / (float(255)) * 360.0; //? 1024 represents the resolution of a full circle
					//value = sin( 3 * (degrees * (M_PI / 180) ));

					// Normalise i value in to j
					j = ((float)i / NUMLEDS) * 360.0;

					length = (j / 60.0); // lower is shorter
					speed = (x / 20.0);  // lower is faster
					value = sin(length + speed);
			
					//value = (value < 0 ? -value : value); // Ensure value is never negative
					rgbValues[i].green = (value * brightness) * settings.intensity;
					rgbValues[mod(i + NUMLEDS / 4, NUMLEDS)].blue = (value * brightness) * settings.intensity;
					rgbValues[mod(i + NUMLEDS / 2, NUMLEDS)].red = (value * brightness) * settings.intensity;
				}
				x = (x + 1);

				// Load pattern in to LEDs
				//? Now that the entire patter has been configured, load all of this data in to the LED Strip memory
				//? - it will be sent after a delay to stop the pattern moving too fast
				for (int i = 0; i < NUMLEDS; ++i)
				{
					rgbStrips.rgbLED[i].setLED(0, rgbValues[i].blue, rgbValues[i].red, rgbValues[i].green);
				}
			}
			if (settings.LEDMode == 1)
			{
				///////////////
				// PATTERN 1 //
				///////////////
				// TODO : use HUE to set color with sine function
				//        use different sine function with intensity
				double degrees, value[2] = { 0 };
				double length[2] = { 0 }, speed[2] = { 0 };
				double j;

				for (int i = 0; i < NUMLEDS; ++i)
				{
					//degrees = float(x + ((255 / NUMLEDS) * i)) / (float(255)) * 360.0; //? 1024 represents the resolution of a full circle
					//value = sin( 3 * (degrees * (M_PI / 180) ));

					// Normalise i value in to j
					j = ((float)i / NUMLEDS) * 360.0;

					length[0] = (j / 60.0); // lower is shorter
					speed[0] = (x / 20.0);  // lower is faster
					value[0] = sin(length[0] + speed[0]);

					length[1] = (j / 90.0); // lower is shorter
					speed[1] = (x / 6.0);  // lower is faster
					value[1] = sin(length[1] + speed[1]);

					//value = (value < 0 ? -value : value); // Ensure value is never negative
					rgbValues[i].green = (value[0] * brightness) * settings.intensity;
					rgbValues[NUMLEDS - mod(i + NUMLEDS / 4, NUMLEDS)].blue = (value[1] * brightness) * settings.intensity;
					rgbValues[mod(i + NUMLEDS / 2, NUMLEDS)].red = (value[0] * brightness) * settings.intensity;
				}
				x = (x + 1);

				// Load pattern in to LEDs
				//? Now that the entire patter has been configured, load all of this data in to the LED Strip memory
				//? - it will be sent after a delay to stop the pattern moving too fast
				for (int i = 0; i < NUMLEDS; ++i)
				{
					rgbStrips.rgbLED[i].setLED(0, rgbValues[i].blue, rgbValues[i].red, rgbValues[i].green);
				}
			}
			else if (settings.LEDMode == 2)
			{
				///////////////
				// PATTERN 2 //
				///////////////
				for (int i = 0; i < NUMLEDS; ++i)
				{
					rgbValues[i].green = ((float)settings.colour_g / 255.0f) * brightness * settings.intensity;
					rgbValues[i].red = ((float)settings.colour_r / 255.0f) * brightness * settings.intensity;
					rgbValues[i].blue = ((float)settings.colour_b / 255.0f) * brightness * settings.intensity;
				}
				
				for (int i = 0; i < NUMLEDS; ++i)
				{
					rgbStrips.rgbLED[i].setLED(0, rgbValues[i].red, rgbValues[i].green, rgbValues[i].blue);
				}
			}
			////////////////////////////
			//// HSV TO RGB PATTERN ////
			////////////////////////////
			else if (settings.LEDMode == 3)
			{
				///////////////
				// PATTERN 3 //
				///////////////
				// Fade around colour wheel
				hsv colourHSV;
				rgbx colourRGB;
				colourHSV.h = ((double)x / (double)INT32_MAX) * 360;
				colourHSV.s = 1.0;
				colourHSV.v = 1.0;

				colourRGB = hsv2rgb(colourHSV);

				// Load colour in
				for (int i = 0; i < NUMLEDS; ++i)
				{
					rgbValues[i].green = colourRGB.g * brightness * settings.intensity;
					rgbValues[i].red = colourRGB.r * brightness * settings.intensity;
					rgbValues[i].blue = colourRGB.b * brightness * settings.intensity;
				}

				x = x + settings.HSVStepSize;

				for (int i = 0; i < NUMLEDS; ++i)
				{
					rgbStrips.rgbLED[i].setLED(0, rgbValues[i].red, rgbValues[i].green, rgbValues[i].blue);
				}
			}
			else if (settings.LEDMode == 4)
			{
				///////////////
				// PATTERN 4 //
				///////////////
				if (settings.counter >= 360)
				{
					settings.counter = 0;
				}
				
				double brightness = sin(settings.counter * (M_PI / 180));
				
				for (int i = 0; i < NUMLEDS; ++i)
				{
					rgbValues[i].green = (float) 1.0f * (float) settings.intensity * brightness;
					rgbValues[i].red = (float) 0.0f * (float) settings.intensity * brightness;
					rgbValues[i].blue = (float) 0.0f * (float) settings.intensity * brightness;
				}
				
				for (int i = 0; i < NUMLEDS; ++i)
				{
					rgbStrips.rgbLED[i].setLED(0, rgbValues[i].red, rgbValues[i].green, rgbValues[i].blue);
				}
			}
			else if (settings.LEDMode == 5)
			{
				///////////////
				// PATTERN 5 //
				///////////////
				if (settings.counter >= 360)
				{
					settings.counter = 0;
				}

				double brightness = sin(settings.counter * (M_PI / 180));

				for (int i = 0; i < NUMLEDS; ++i)
				{
					rgbValues[i].green = (float) 0.0f * settings.intensity * brightness;
					rgbValues[i].red = (float) 1.0f * settings.intensity * brightness;
					rgbValues[i].blue = (float) 0.0f * settings.intensity * brightness;
				}

				for (int i = 0; i < NUMLEDS; ++i)
				{
					rgbStrips.rgbLED[i].setLED(0, rgbValues[i].red, rgbValues[i].green, rgbValues[i].blue);
				}
			}
			else if (settings.LEDMode == 6)
			{
				///////////////
				// PATTERN 6 //
				///////////////
				if (settings.counter >= 360)
				{
					settings.counter = 0;
				}

				double brightness = sin(settings.counter * (M_PI / 180));

				for (int i = 0; i < NUMLEDS; ++i)
				{
					rgbValues[i].green = (float) 0.0f * settings.intensity * brightness;
					rgbValues[i].red = (float) 0.0f * settings.intensity * brightness;
					rgbValues[i].blue = (float) 1.0f * settings.intensity * brightness;
				}

				for (int i = 0; i < NUMLEDS; ++i)
				{
					rgbStrips.rgbLED[i].setLED(0, rgbValues[i].red, rgbValues[i].green, rgbValues[i].blue);
				}
			}
			else if (settings.LEDMode == 7)
			{
				///////////////
				// PATTERN 7 //
				///////////////
				if (settings.counter >= 360)
				{
					settings.counter = 0;
				}

				double brightness = sin(settings.counter * (M_PI / 180));

				for (int i = 0; i < NUMLEDS; ++i)
				{
					rgbValues[i].green = (float) 1.0f * settings.intensity * brightness;
					rgbValues[i].red = (float) 0.0f * settings.intensity * brightness;
					rgbValues[i].blue = (float) 1.0f * settings.intensity * brightness;
				}

				for (int i = 0; i < NUMLEDS; ++i)
				{
					rgbStrips.rgbLED[i].setLED(0, rgbValues[i].red, rgbValues[i].green, rgbValues[i].blue);
				}
			}
			
			settings.calculationReady = true; // Allow the DMA system to update LEDs
		}
	}
}

////////////////////////////////
// Interrupt Service Routines //
////////////////////////////////
extern "C" void DMA2_Stream6_IRQHandler(void)
{
	// Check for appropriate flag
	if (DMA_GetITStatus(DMA2_Stream6, DMA_IT_TCIF6) == SET)
	{
		TIM_Cmd(TIM1, DISABLE); // Disable Timer
		DMA_ClearITPendingBit(DMA2_Stream6, DMA_IT_TCIF6); // Clear flag
		isDMAOccupied = false; // Allow program to move on
	}
}
extern "C" void DMA2_Stream5_IRQHandler(void)
{
	// Check for appropriate flag
	if (DMA_GetITStatus(DMA2_Stream5, DMA_IT_TCIF5) == SET)
	{
		TIM_Cmd(TIM1, DISABLE); // Disable Timer
		DMA_ClearITPendingBit(DMA2_Stream5, DMA_IT_TCIF5); // Clear flag
		GPIO_ResetBits(GPIOA, 0x00FF); // Ensure port goes low
		isDMAOccupied = false; // Allow program to move on
	}
}

// Main Drive Control through interrupt / Timer 4
extern "C" void TIM4_IRQHandler()							// Use the ISR for Timer 2 (specified in startup_stm32f4xx.c) - extern "C" is used due to C++ application
{
	static uint8_t driveDecay;	// Set a variable to decay each command
	static uint8_t count = 0;	// Counter variable
	static uint8_t updateMillis;
	char reportString[196] = "";
	uint8_t reportLength = 0;
	static uint16_t buttoncount = 0;
	
	if (TIM_GetITStatus(TIM4, TIM_IT_Update) != RESET)		// Check that TIM_IT_Update is valid (is this needed?)
	{
		TIM_ClearITPendingBit(TIM4, TIM_IT_Update);			// Clear interrupt bit (software doesn't automatically do it)

		// Check for button push
		if ((GPIO_ReadInputData(GPIOA) & 0x0001) == 1)
		{
			buttoncount++;
			if (buttoncount == 10)
			{
				settings.LEDMode = (settings.LEDMode + 1) % 7;
			}
		}
		else
		{
			buttoncount = 0;
		}

		if (count == updateMillis)
		{
			settings.counter++;
			count = 0;
			settings.calculationReady = false;
			
			// Wait for completion
			while (DMA2_Stream2->CR & DMA_SxCR_EN);
			// Start DMA
			startGPIODMA((uint32_t)&rgbStrips);
			
			// only refresh once every count cycle, saves instructions
			updateMillis = settings.getUpdateMillis();
		}
		else
		{
			if (count == updateMillis - 2)
			{
				// Do preparation routines
			}

			++count;
		}
	}
}

////////////////////////////////
// Interrupt Service Routines //
////////////////////////////////
// ISR For USART1 (BT)
extern "C" void USART1_IRQHandler()
{
	static char cTX = '\0';
	static char cRX = '\0';
	//static char cRXCommand[2] = "\0"; // Max command length = 1
	//static char ucIterator = 0;

	// TX Interrupt
	if (USART_GetITStatus(USART1, USART_IT_TXE) != RESET)
	{
		// Send TX Data (Buffer manages interrupt)
		bt.TXBuffer.GetOldest(&cTX);
		USART_SendData(USART1, cTX);
		// Interrupt is automatically cleared upon write
	}
	// RX Interrupt
	if (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
	{
		// Get RX Data
		cRX = USART_ReceiveData(USART1);
		bt.BT_Recieve(cRX);
		// Interrupt is automatically cleared upon read
	}
}

///////////////
// Functions //
///////////////
void InitClockOutput()
{
	GPIO_InitTypeDef GPIO_InitStruct;

	// Set up MCO clock output source
	RCC_MCO1Config(RCC_MCO1Source_HSE, RCC_MCO1Div_1);		// Set MCO1 source to HSE (divide by 1 = 8MHz)
	RCC_MCO2Config(RCC_MCO2Source_SYSCLK, RCC_MCO2Div_1);	// Set MCO1 source to SYSCLK (divide by 5 = 33.6MHz)

	// Enable AF (MCO1) on PA8
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);	// Enable clock on port A
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_8;				// Pick which pin(s) to init - Pin 8
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;			// Set pin(s) mode to Alternate Function
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;		// Set pin(s) speed to maximum - 50MHz
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;		// Disable Pull-up / Pull-down on pin(s)
	GPIO_Init(GPIOA, &GPIO_InitStruct);					// Set data on selected port using struct

	// Enable AF (MCO2) on PC9
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);	// Enable clock on port C
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_9;				// Pick which pin(s) to init - Pin 9
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;			// Set pin(s) mode to Alternate Function
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;		// Set pin(s) speed to maximum - 50MHz
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;		// Disable Pull-up / Pull-down on pin(s)
	GPIO_Init(GPIOC, &GPIO_InitStruct);					// Set data on selected port using struct
}

void InitMainTimer(uint16_t dTime)
{
	// Set up for 50Hz interrupt (Motor Control)
	// - http://dohzer.blogspot.com.au/2013/04/stm32-system-and-timer-clock_7.html
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);	// Enable Timer 2 block (Give it a clock)
	TIM_TimeBaseInitTypeDef timerInitStruct;
	timerInitStruct.TIM_Prescaler = 84;						// Set timer prescaler to 40000 (sysclock divider)
	//  - Timer Tick Frequency => 84MHz / (83 + 1) = 1MHz
	timerInitStruct.TIM_Period = 1060 * dTime;				// Set timer period (reload value)
	//  - 1/2 Period => 84MHz / (4199 + 1) = 10kHz
	timerInitStruct.TIM_CounterMode = TIM_CounterMode_Up;	// Set timer counter mode to count up
	timerInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;		// Set clock division
	timerInitStruct.TIM_RepetitionCounter = 0;				// Set number of timer repetitions to 0 before update event is generated and number resets (only for TIM1 and TIM8)
	TIM_TimeBaseInit(TIM4, &timerInitStruct);				// Load init data into Timer 2
	TIM_Cmd(TIM4, ENABLE);										// Enable Timer2

	TIM_ClearITPendingBit(TIM4, TIM_IT_Update);				// Clear Timer 2 interrupt bit
	TIM_ITConfig(TIM4, TIM_IT_Update, ENABLE);				// Enable Timer 2 Interrupt

	NVIC_InitTypeDef nvicStructure;
	nvicStructure.NVIC_IRQChannel = TIM4_IRQn;				// Set the IRQ channel to TIM2
	nvicStructure.NVIC_IRQChannelPreemptionPriority = 0;	// Set the interrupt priority, lower is more important
	nvicStructure.NVIC_IRQChannelSubPriority = 1;			// Set the interrupt sub-priority, lower is more important
	nvicStructure.NVIC_IRQChannelCmd = ENABLE;				// Set the interrupt to enabled
	NVIC_Init(&nvicStructure);								// Send the struct to init the interrupt
}

void initGPIO()
{
	// Initialise Port A {0 - 7}
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.GPIO_Pin = 0x00FF;						// Pick which pin(s) to init - Pin 7 downto 0
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;				// Set pin(s) mode to output
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;				// Set output type to Push/Pull
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;			// Set internal resistor to off
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;			// 100MHz pin clock for testing
	GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_SetBits(GPIOA, 0x00);							// Ensure all pins are low... good place to start

	// Initialise Port B {0 - 7}
//	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
//	GPIO_InitStruct.GPIO_Pin = 0x00FF;						// Pick which pin(s) to init - Pin 7 downto 0
//	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;				// Set pin(s) mode to output
//	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;				// Set output type to Push/Pull
//	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;			// Set internal resistor to off
//	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;			// 100MHz pin clock for testing
//	GPIO_Init(GPIOB, &GPIO_InitStruct);
//
//	GPIO_SetBits(GPIOB, 0x00);							// Ensure all pins are low... good place to start
}

void initGPIODMA()
{
	// Enable DMA Clock
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);

	// Init DMA2, Stream6, Chan0 for GPIO
	DMA_InitTypeDef DMA_InitStructure;
	DMA_InitStructure.DMA_Channel = DMA_Channel_6;							// Trigger on TIM1_CH{1-3}
	DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)&rgbStrips;			// Set memory location (FROM)
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&(GPIOA->ODR);		// Set GPIO location (TO)
	//DMA_InitStructure.DMA_BufferSize = sizeof(rgb) * NUMLEDS + sizeof(uint8_t) * 24;				// Amount of data to send (rgb = 24 * char)
	DMA_InitStructure.DMA_BufferSize = sizeof(rgbStrip);
	
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;					// Increment memory addr after each read
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;		// Don't increment away from peripheral address
	
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;	// Send data byte by byte
	
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;							// Not a circular transmit, only go over data once
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;						// Set priority... high seems necessary
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_Full;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(DMA2_Stream5, &DMA_InitStructure);								// Sent init data to DMA2

	// Enable interrupts for DMA2 Stream6
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = DMA2_Stream5_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	DMA_ITConfig(DMA2_Stream5, DMA_IT_TC, ENABLE);		// Enable DMA interrupt for transfer complete
	DMA_ITConfig(DMA2_Stream5, DMA_IT_HT, DISABLE);		// Disable DMA interrupt for half transfer complete
	DMA_ITConfig(DMA2_Stream5, DMA_IT_HTIF5, DISABLE);
	
}

void initGPIODMATimer()
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);	// Enable Timer 1 block
	TIM_TimeBaseInitTypeDef timerInitStruct;
	timerInitStruct.TIM_Prescaler = 1 - 1;					// Set timer prescaler to 0
	timerInitStruct.TIM_Period = 69 - 1;					// Set timer period (reload value) // (approx 1.25us - #69)
															//  ** TIMER HAS BEEN LOWERED TO 940ns, can go lower but gets unstable.
	timerInitStruct.TIM_CounterMode = TIM_CounterMode_Up;	// Set timer counter mode to count up
	timerInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;		// Set clock division
	timerInitStruct.TIM_RepetitionCounter = 0;				// Set number of timer repetitions to 0 before update event is generated and number resets (only for TIM1 and TIM8)
	TIM_TimeBaseInit(TIM1, &timerInitStruct);				// Load init data into Timer 1

	// Configure Chan1 for First bit - Trigger DMA at '0' (0us)
	TIM_OCInitTypeDef outputChannelInit = { 0, };
	outputChannelInit.TIM_OCMode = TIM_OCMode_Active;
	outputChannelInit.TIM_Pulse = 0;						// 67 = 1, 34 = 0
	outputChannelInit.TIM_OutputState = TIM_OutputState_Enable;
	outputChannelInit.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OC1Init(TIM1, &outputChannelInit);
	TIM_OC1PreloadConfig(TIM1, TIM_OCPreload_Enable);
	
	// Configure Chan2 for Second bit - Trigger DMA at '34' (0.35us)
	outputChannelInit = { 0, };
	outputChannelInit.TIM_OCMode = TIM_OCMode_Active;
	outputChannelInit.TIM_Pulse = 34;						// 67 = 1, 34 = 0
	outputChannelInit.TIM_OutputState = TIM_OutputState_Enable;
	outputChannelInit.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OC2Init(TIM1, &outputChannelInit);
	TIM_OC2PreloadConfig(TIM1, TIM_OCPreload_Enable);
	
	// Configure Chan3 for Third bit - Trigger DMA at '67' (0.7us)
	outputChannelInit = { 0, };
	outputChannelInit.TIM_OCMode = TIM_OCMode_Active;
	outputChannelInit.TIM_Pulse = 67;						// 67 = 1, 34 = 0
	outputChannelInit.TIM_OutputState = TIM_OutputState_Enable;
	outputChannelInit.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OC3Init(TIM1, &outputChannelInit);
	TIM_OC3PreloadConfig(TIM1, TIM_OCPreload_Enable);
	
	// Connect Timer Channels to DMA
//	TIM_SelectCCDMA(TIM1, ENABLE);
//	TIM_DMACmd(TIM1, TIM_DMA_CC1, ENABLE);
//	TIM_DMACmd(TIM1, TIM_DMA_CC2, ENABLE);
//	TIM_DMACmd(TIM1, TIM_DMA_CC3, ENABLE);
	TIM_DMACmd(TIM1, TIM_DMA_Update, ENABLE);
}

void startGPIODMA(uint32_t address)
{
	// Wait for previous operation to finish
	while (DMA2_Stream2->CR & DMA_SxCR_EN);
	
	// Clear all remaining flags
	DMA_ClearFlag(DMA2_Stream5, DMA_FLAG_FEIF5 | DMA_FLAG_DMEIF5 | DMA_FLAG_TEIF5 | DMA_FLAG_HTIF5 | DMA_FLAG_TCIF5);
	
	// Set the address of the new data to send
	DMA2_Stream5->M0AR = address;
	
	// Enable the DMA stream
	DMA_Cmd(DMA2_Stream5, ENABLE);
	
	// Enable the timer (that triggers the stream)
	TIM_Cmd(TIM1, ENABLE);
}

int mod(int x, int m) {
	int r = x%m;
	return r<0 ? r + m : r;
}