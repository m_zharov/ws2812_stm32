#include <stm32f4xx_gpio.h>
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_tim.h> // Timer
#include <stm32f4xx_usart.h>
#include <stm32f4xx_spi.h>
#include <stm32f4xx_dma.h>
#include <misc.h> // NVIC
#include <math.h>
#include <tgmath.h>
#include <string.h>
#include <cstdlib>

#include <stdio.h> // Debug messages

/////////////
// Defines //
/////////////
#define SPI_LED_BLANK 0x00
#define SPI_LED_0H 0xC0
#define SPI_LED_1H 0xF0

#define UART_LED_BLANK 0x00
#define UART_LED_0H 0xC0
#define UART_LED_1H 0xF8

#define NUMLEDS 150
#define NUMLOBES 6

/////////////////////////
// Static Const Memory //
/////////////////////////
static const int LEDZero = 0x0000FF;

//????????????????//
// Struct to hold green, red and blue data for a frame
// - This struct gets the planned colours loaded in before being sent to the rgb struct.
// - Any modification to a frame should be implemented in here and loaded across.
typedef struct
{
	uint8_t green;
	uint8_t red;
	uint8_t blue;
} rgbCalc;
//????????????????//

typedef struct
{
	// Each encoded bit requires three bits to be stored
	//   '0' = 100  ~ 0.35us high, 0.80us low
	//   '1' = 110  ~ 0.70us high, 0.65us low
	// As the DMA controller can control an entire port, 8 pins will be used in parallel allowing for 8 streams.
	// This means that in the 24 bits stored per colour, 3 will be used per port ~ green[0]{0}, green[1]{0}, green[2]{0}
	
	uint8_t green[24];	// 3 bits per digit, 8 digits per colour = 24
	uint8_t red[24];
	uint8_t blue[24];
	
	// Zero ALL leds (Also resets first and third bits for signal)
	bool setZero()
	{
		for (int i = 0; i < 72; i = i + 3)
		{
			memcpy(&green[i], &LEDZero, 3);
		}
	}
	// Zero specific LED in stream
	bool setZero(uint8_t stream)
	{
		for (int i = 0; i < 72; i = i + 3)
		{
			green[i] |= (0x01 << stream);		// High
			green[i + 1] &= ~(0x01 << stream);	// Low
			green[i + 2] &= ~(0x01 << stream);  // Low
		}
	}
	// Fade specific LEDs in stream by amount
	bool dim(uint8_t stream, uint8_t amount)
	{
		uint8_t tempGreen = 0, tempRed = 0, tempBlue = 0;

		// Copy Old Value to char
		for (int i = 0; i < 8; ++i)
		{
			tempGreen |= ((green[1 + (i * 3)] >> stream) & 0x01) << i;
		}
		
		// Copy Old Value to char
		for (int i = 0; i < 8; ++i)
		{
			tempRed |= ((red[1 + (i * 3)] >> stream) & 0x01) << i;
		}

		// Copy Old Value to char
		for (int i = 0; i < 8; ++i)
		{
			tempBlue |= ((blue[1 + (i * 3)] >> stream) & 0x01) << i;
		}

		// subtract 'amount' from LED
		tempGreen -= amount;
		tempRed -= amount;
		tempBlue -= amount;

		setLED(stream, tempRed, tempGreen, tempBlue);
	}

	// Stream = 0 - 7, 
	bool setLED(uint8_t stream, uint8_t inRed, uint8_t inGreen, uint8_t inBlue)
	{
		// Check for errors
		if (stream > 8)
		{
			return false;
		}
		
		///////////////
		// VARIABLES //
		///////////////
		// Temporary colour array
		uint8_t temp[8];
		
		///////////
		// Green //
		///////////
		// Split colour in to easier to manage array
		for (int i = 7; i >= 0; i--) {
			temp[i] = 0x01 & inGreen;
			inGreen = inGreen >> 1; 
		}
		// Load values in to appropriate LED bits
		for (int i = 0; i < 8; ++i)
		{
			if (temp[i] == 1)
			{
				green[1 + (i * 3)] |= (1 << stream);
			}
			else
			{
				green[1 + (i * 3)] &= ~(1 << stream);
			}
		}
		
		/////////
		// Red //
		/////////
		// Split colour in to easier to manage array
		for (int i = 7; i >= 0; i--) {
			temp[i] = 0x01 & inRed;
			inRed = inRed >> 1; 
		}
		// Load values in to appropriate LED bits
		for (int i = 0; i < 8; ++i)
		{
			if (temp[i] == 1)
			{
				red[1 + (i * 3)] |= (1 << stream);
			}
			else
			{
				red[1 + (i * 3)] &= ~(1 << stream);
			}
		}
		
		//////////
		// Blue //
		//////////
		// Split colour in to easier to manage array
		for (int i = 7; i >= 0; i--) {
			temp[i] = 0x01 & inBlue;
			inBlue = inBlue >> 1; 
		}
		// Load values in to appropriate LED bits
		for (int i = 0; i < 8; ++i)
		{
			if (temp[i] == 1)
			{
				blue[1 + (i * 3)] |= (1 << stream);
			}
			else
			{
				blue[1 + (i * 3)] &= ~(1 << stream);
			}
		}
		
		return true;
	}
} rgb;

typedef struct
{
	// This struct exists to hold an easy collection of LED data, along with guaranteed blank space at the start AND end of transmission, keeps things clean.
	uint8_t blankStart[24] = { 0 };	// Blank array to guarantee long nothingness
	rgb rgbLED[NUMLEDS];
	uint8_t blankEnd[24] = { 0 };
} rgbStrip;


/////////////////////////
// Function Prototypes //
/////////////////////////
void InitClockOutput();
void initGPIO();
void initGPIODMA();
void initGPIODMATimer();
void startGPIODMA(uint32_t address);
int mod(int x, int m);


//////////////////////
// Global Variables //
//////////////////////
rgbStrip rgbStrips;
bool isDMAOccupied = false;

int main()
{
	// Initialise LED array to have appropriate starting values
	//  - All off
	for (int i = 0; i < NUMLEDS; ++i)
	{
		rgbStrips.rgbLED[i].setZero();
	}

	// Init IO
	//InitClockOutput();
	// GPIO DMA Method
	initGPIO();
	initGPIODMA();
	initGPIODMATimer();

	//????????????????//
	// Variables for storing pattern
	rgbCalc rgbValues[NUMLEDS];		//? This variable contains the colour intensity data described waaay above
	// Pattern 1
	int lobe[NUMLOBES];				//? This array of integers contains the centre positions of each 'specular lobe'
	// Pattern 2
	int x = 0, y = 23;

	// Random Numbers
	double random = 0, oldRandom = 0;
	double brightness = 0;
	bool rising = true;

	//? To start the pattern moving each lobe is placed equidistant along the strip.
	for (int i = 0; i < NUMLOBES; ++i)
	{
		lobe[i] = (NUMLEDS / NUMLOBES) * i;
	}
	//????????????????//

	// Run main loop
	for (;;)
	{
		//////////////////////
		// GPIO DMA Soluion //
		//////////////////////
		
		// Wait for completion
		while (DMA2_Stream2->CR & DMA_SxCR_EN);
		// Start DMA
		startGPIODMA((uint32_t)&rgbStrips);

		/////////////////////////
		// Update Pattern Data //
		/////////////////////////
		// This method has a fatal flaw, overlapping lobes will not be drawn, only the closest (due to IF and ELSE IF)
		// An additional problem is that if lobes are close tails will be overwritten.
		// - A solution would be to set rgbValues[i].green = rgbValues[i].green + 150;
		//    - (same for green and blue)
		//    - (the current method is easier to type but more absolute -- rgbValues[i] = { 150, 0, 0 };

		//? Turn off ALL leds - Start with a clean slate by filling the entire array with 0s
		for (int i = 0; i < NUMLEDS; ++i)
		{
			rgbValues[i] = { 0 };
		}

		
		///////////////
		// PATTERN 1 //
		///////////////
		//? colour in new desired colours.
//		for (int i = 0; i < NUMLEDS; ++i)						//? For each LED...
//		{
//			if (i == lobe[0])									//? if the LED is in a lobe location...
//			{
//				rgbValues[mod(i - 4, NUMLEDS)] = { 7, 0, 0 };	//? gently trail the centre led (wrapping around from 105 to 0 if necessary - "int mod(...)" defined below...
//				rgbValues[mod(i - 3, NUMLEDS)] = { 15, 0, 0 };
//				rgbValues[mod(i - 2, NUMLEDS)] = { 40, 0, 0 };
//				rgbValues[mod(i - 1, NUMLEDS)] = { 80, 0, 0 };
//				rgbValues[i] = { 150, 0, 0 };					//? Colour in the centre LED
//				rgbValues[mod(i + 1, NUMLEDS)] = { 80, 0, 0 };	//? gently lead the centre LED...
//				rgbValues[mod(i + 2, NUMLEDS)] = { 40, 0, 0 };
//				rgbValues[mod(i + 3, NUMLEDS)] = { 15, 0, 0 };
//				rgbValues[mod(i + 4, NUMLEDS)] = { 7, 0, 0 };
//				
//			}
//			else if (i == lobe[1])								//? Same again for the next lobe
//			{
//				rgbValues[mod(i - 4, NUMLEDS)] = { 4, 4, 0 };
//				rgbValues[mod(i - 3, NUMLEDS)] = { 8, 8, 0 };
//				rgbValues[mod(i - 2, NUMLEDS)] = { 25, 25, 0 };
//				rgbValues[mod(i - 1, NUMLEDS)] = { 50, 50, 0 };
//				rgbValues[i] = { 80, 80, 0 };
//				rgbValues[mod(i + 1, NUMLEDS)] = { 50, 50, 0 };
//				rgbValues[mod(i + 2, NUMLEDS)] = { 25, 25, 0 };
//				rgbValues[mod(i + 3, NUMLEDS)] = { 8, 8, 0 };
//				rgbValues[mod(i + 4, NUMLEDS)] = { 4, 4, 0 };
//			}
//			else if (i == lobe[2])
//			{
//				rgbValues[mod(i - 4, NUMLEDS)] = { 0, 7, 0 };
//				rgbValues[mod(i - 3, NUMLEDS)] = { 0, 15, 0 };
//				rgbValues[mod(i - 2, NUMLEDS)] = { 0, 40, 0 };
//				rgbValues[mod(i - 1, NUMLEDS)] = { 0, 80, 0 };
//				rgbValues[i] = { 0, 150, 0 };		  
//				rgbValues[mod(i + 1, NUMLEDS)] = { 0, 80, 0 };
//				rgbValues[mod(i + 2, NUMLEDS)] = { 0, 40, 0 };
//				rgbValues[mod(i + 3, NUMLEDS)] = { 0, 15, 0 };
//				rgbValues[mod(i + 4, NUMLEDS)] = { 0, 7, 0 };
//			}
//			else if (i == lobe[3])
//			{
//				rgbValues[mod(i - 4, NUMLEDS)] = { 0, 4, 4 };
//				rgbValues[mod(i - 3, NUMLEDS)] = { 0, 8, 8 };
//				rgbValues[mod(i - 2, NUMLEDS)] = { 0, 25, 25 };
//				rgbValues[mod(i - 1, NUMLEDS)] = { 0, 50, 50 };
//				rgbValues[i] = { 0, 80, 80 };
//				rgbValues[mod(i + 1, NUMLEDS)] = { 0, 50, 50 };
//				rgbValues[mod(i + 2, NUMLEDS)] = { 0, 25, 25 };
//				rgbValues[mod(i + 3, NUMLEDS)] = { 0, 8, 8 };
//				rgbValues[mod(i + 4, NUMLEDS)] = { 0, 4, 4 };
//			}
//			else if (i == lobe[4])
//			{
//				rgbValues[mod(i - 4, NUMLEDS)] = { 0, 0, 7 };
//				rgbValues[mod(i - 3, NUMLEDS)] = { 0, 0, 15 };
//				rgbValues[mod(i - 2, NUMLEDS)] = { 0, 0, 40 };
//				rgbValues[mod(i - 1, NUMLEDS)] = { 0, 0, 80 };
//				rgbValues[i] = { 0, 0, 150 };
//				rgbValues[mod(i + 1, NUMLEDS)] = { 0, 0, 80 };
//				rgbValues[mod(i + 2, NUMLEDS)] = { 0, 0, 40 };
//				rgbValues[mod(i + 3, NUMLEDS)] = { 0, 0, 15 };
//				rgbValues[mod(i + 4, NUMLEDS)] = { 0, 0, 7 };
//			}
//			else if (i == lobe[5])
//			{
//				rgbValues[mod(i - 4, NUMLEDS)] = { 4, 0, 4 };
//				rgbValues[mod(i - 3, NUMLEDS)] = { 8, 0, 8 };
//				rgbValues[mod(i - 2, NUMLEDS)] = { 25, 0, 25 };
//				rgbValues[mod(i - 1, NUMLEDS)] = { 50, 0, 50 };
//				rgbValues[i] = { 80, 0, 80 };
//				rgbValues[mod(i + 1, NUMLEDS)] = { 50, 0, 50 };
//				rgbValues[mod(i + 2, NUMLEDS)] = { 25, 0, 25 };
//				rgbValues[mod(i + 3, NUMLEDS)] = { 8, 0, 8 };
//				rgbValues[mod(i + 4, NUMLEDS)] = { 4, 0, 4 };
//			}
//		}
//
//		// Increment pattern for next run
//		//? This is the bit that animates the entire string.
//		//? - if you want to skip blocks or have a more complicated position function, here seems like a good place
//		for (int i = 0; i < NUMLOBES; ++i)
//		{
//			lobe[i] = (lobe[i] + 1) % (NUMLEDS);
//		}
		

		///////////////
		// PATTERN 2 //
		///////////////
		// TODO : use HUE to set color with sine function
		//        use different sine function with intensity
		double degrees, value;
		double length, speed;
		double j;
		
		if (rising)
		{
			brightness = brightness + 0.1;

			if (brightness > 120)
				rising = false;
		}
		else
		{
			brightness = brightness - 0.1;
			if (brightness < 1)
				rising = true;
		}
		for (int i = 0; i < NUMLEDS; ++i)
		{
			//degrees = float(x + ((255 / NUMLEDS) * i)) / (float(255)) * 360.0; //? 1024 represents the resolution of a full circle
			//value = sin( 3 * (degrees * (M_PI / 180) ));

			// Normalise i value in to j
			j = ((float)i / NUMLEDS) * 360.0;

			length = (j / 60.0); // lower is shorter
			speed = (x / 20.0);  // lower is faster
			value = sin(length + speed);
			
			//value = (value < 0 ? -value : value); // Ensure value is never negative
			rgbValues[i].green = value * (20 + brightness);
			rgbValues[mod(i + 30, NUMLEDS)].blue = value * (20 + brightness);
			rgbValues[mod(i + 60, NUMLEDS)].red = value * (20 + brightness);
		}
		x = (x + 1);

		///////////////
		// PATTERN 2 //
		///////////////


		// Load pattern in to LEDs
		//? Now that the entire patter has been configured, load all of this data in to the LED Strip memory
		//? - it will be sent after a delay to stop the pattern moving too fast
		for (int i = 0; i < NUMLEDS; ++i)
		{
			rgbStrips.rgbLED[i].setLED(0, rgbValues[i].blue, rgbValues[i].red, rgbValues[i].green);
			rgbStrips.rgbLED[mod(i + 35, NUMLEDS)].setLED(1, rgbValues[i].green, rgbValues[i].blue, rgbValues[i].red);
		}

		//? ** This is the delay... it isn't really calibrated, it just wastes time.
		//? - If the program above is much longer this delay would slow the update down quite a lot... it looks like maybe ?100ms? currently.
		for (int i = 0; i < 200; ++i)
		{

		}
	}
}

////////////////////////////////
// Interrupt Service Routines //
////////////////////////////////
extern "C" void DMA2_Stream6_IRQHandler(void)
{
	// Check for appropriate flag
	if (DMA_GetITStatus(DMA2_Stream6, DMA_IT_TCIF6) == SET)
	{
		TIM_Cmd(TIM1, DISABLE); // Disable Timer
		DMA_ClearITPendingBit(DMA2_Stream6, DMA_IT_TCIF6); // Clear flag
		isDMAOccupied = false; // Allow program to move on
	}
}
extern "C" void DMA2_Stream5_IRQHandler(void)
{
	// Check for appropriate flag
	if (DMA_GetITStatus(DMA2_Stream5, DMA_IT_TCIF5) == SET)
	{
		TIM_Cmd(TIM1, DISABLE); // Disable Timer
		DMA_ClearITPendingBit(DMA2_Stream5, DMA_IT_TCIF5); // Clear flag
		isDMAOccupied = false; // Allow program to move on
	}
	
	// Clear all remaining flags
	//DMA_ClearFlag(DMA2_Stream5, DMA_FLAG_FEIF5 | DMA_FLAG_DMEIF5 | DMA_FLAG_TEIF5 | DMA_FLAG_HTIF5 | DMA_FLAG_TCIF5);
}

///////////////
// Functions //
///////////////
void InitClockOutput()
{
	GPIO_InitTypeDef GPIO_InitStruct;

	// Set up MCO clock output source
	RCC_MCO1Config(RCC_MCO1Source_HSE, RCC_MCO1Div_1);		// Set MCO1 source to HSE (divide by 1 = 8MHz)
	RCC_MCO2Config(RCC_MCO2Source_SYSCLK, RCC_MCO2Div_1);	// Set MCO1 source to SYSCLK (divide by 5 = 33.6MHz)

	// Enable AF (MCO1) on PA8
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);	// Enable clock on port A
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_8;				// Pick which pin(s) to init - Pin 8
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;			// Set pin(s) mode to Alternate Function
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;		// Set pin(s) speed to maximum - 50MHz
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;		// Disable Pull-up / Pull-down on pin(s)
	GPIO_Init(GPIOA, &GPIO_InitStruct);					// Set data on selected port using struct

	// Enable AF (MCO2) on PC9
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);	// Enable clock on port C
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_9;				// Pick which pin(s) to init - Pin 9
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;			// Set pin(s) mode to Alternate Function
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;		// Set pin(s) speed to maximum - 50MHz
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;		// Disable Pull-up / Pull-down on pin(s)
	GPIO_Init(GPIOC, &GPIO_InitStruct);					// Set data on selected port using struct
}

void initGPIO()
{
	// Initialise Port A {0 - 7}
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.GPIO_Pin = 0x00FF;						// Pick which pin(s) to init - Pin 7 downto 0
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;				// Set pin(s) mode to output
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;				// Set output type to Push/Pull
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;			// Set internal resistor to off
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;			// 100MHz pin clock for testing
	GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	GPIO_SetBits(GPIOA, 0x00);							// Ensure all pins are low... good place to start
}

void initGPIODMA()
{
	// Enable DMA Clock
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);

	// Init DMA2, Stream6, Chan0 for GPIO
	DMA_InitTypeDef DMA_InitStructure;
	DMA_InitStructure.DMA_Channel = DMA_Channel_6;							// Trigger on TIM1_CH{1-3}
	DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)&rgbStrips;			// Set memory location (FROM)
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&(GPIOA->ODR);		// Set GPIO location (TO)
	//DMA_InitStructure.DMA_BufferSize = sizeof(rgb) * NUMLEDS + sizeof(uint8_t) * 24;				// Amount of data to send (rgb = 24 * char)
	DMA_InitStructure.DMA_BufferSize = sizeof(rgbStrip);
	
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;					// Increment memory addr after each read
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;		// Don't increment away from peripheral address
	
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;	// Send data byte by byte
	
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;							// Not a circular transmit, only go over data once
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;						// Set priority... high seems necessary
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_Full;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(DMA2_Stream5, &DMA_InitStructure);								// Sent init data to DMA2

	// Enable interrupts for DMA2 Stream6
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = DMA2_Stream5_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	DMA_ITConfig(DMA2_Stream5, DMA_IT_TC, ENABLE);		// Enable DMA interrupt for transfer complete
	DMA_ITConfig(DMA2_Stream5, DMA_IT_HT, DISABLE);		// Disable DMA interrupt for half transfer complete
	DMA_ITConfig(DMA2_Stream5, DMA_IT_HTIF5, DISABLE);
	
}

void initGPIODMATimer()
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);	// Enable Timer 1 block
	TIM_TimeBaseInitTypeDef timerInitStruct;
	timerInitStruct.TIM_Prescaler = 1 - 1;					// Set timer prescaler to 0
	timerInitStruct.TIM_Period = 69 - 1;					// Set timer period (reload value) // (approx 1.25us - #69)
															//  ** TIMER HAS BEEN LOWERED TO 940ns, can go lower but gets unstable.
	timerInitStruct.TIM_CounterMode = TIM_CounterMode_Up;	// Set timer counter mode to count up
	timerInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;		// Set clock division
	timerInitStruct.TIM_RepetitionCounter = 0;				// Set number of timer repetitions to 0 before update event is generated and number resets (only for TIM1 and TIM8)
	TIM_TimeBaseInit(TIM1, &timerInitStruct);				// Load init data into Timer 1

	// Configure Chan1 for First bit - Trigger DMA at '0' (0us)
	TIM_OCInitTypeDef outputChannelInit = { 0, };
	outputChannelInit.TIM_OCMode = TIM_OCMode_Active;
	outputChannelInit.TIM_Pulse = 0;						// 67 = 1, 34 = 0
	outputChannelInit.TIM_OutputState = TIM_OutputState_Enable;
	outputChannelInit.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OC1Init(TIM1, &outputChannelInit);
	TIM_OC1PreloadConfig(TIM1, TIM_OCPreload_Enable);
	
	// Configure Chan2 for Second bit - Trigger DMA at '34' (0.35us)
	outputChannelInit = { 0, };
	outputChannelInit.TIM_OCMode = TIM_OCMode_Active;
	outputChannelInit.TIM_Pulse = 34;						// 67 = 1, 34 = 0
	outputChannelInit.TIM_OutputState = TIM_OutputState_Enable;
	outputChannelInit.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OC2Init(TIM1, &outputChannelInit);
	TIM_OC2PreloadConfig(TIM1, TIM_OCPreload_Enable);
	
	// Configure Chan3 for Third bit - Trigger DMA at '67' (0.7us)
	outputChannelInit = { 0, };
	outputChannelInit.TIM_OCMode = TIM_OCMode_Active;
	outputChannelInit.TIM_Pulse = 67;						// 67 = 1, 34 = 0
	outputChannelInit.TIM_OutputState = TIM_OutputState_Enable;
	outputChannelInit.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OC3Init(TIM1, &outputChannelInit);
	TIM_OC3PreloadConfig(TIM1, TIM_OCPreload_Enable);
	
	// Connect Timer Channels to DMA
//	TIM_SelectCCDMA(TIM1, ENABLE);
//	TIM_DMACmd(TIM1, TIM_DMA_CC1, ENABLE);
//	TIM_DMACmd(TIM1, TIM_DMA_CC2, ENABLE);
//	TIM_DMACmd(TIM1, TIM_DMA_CC3, ENABLE);
	TIM_DMACmd(TIM1, TIM_DMA_Update, ENABLE);
}

void startGPIODMA(uint32_t address)
{
	// Wait for previous operation to finish
	while (DMA2_Stream2->CR & DMA_SxCR_EN);
	
	// Clear all remaining flags
	DMA_ClearFlag(DMA2_Stream5, DMA_FLAG_FEIF5 | DMA_FLAG_DMEIF5 | DMA_FLAG_TEIF5 | DMA_FLAG_HTIF5 | DMA_FLAG_TCIF5);
	
	// Set the address of the new data to send
	DMA2_Stream5->M0AR = address;
	
	// Enable the DMA stream
	DMA_Cmd(DMA2_Stream5, ENABLE);
	
	// Enable the timer (that triggers the stream)
	TIM_Cmd(TIM1, ENABLE);
}

int mod(int x, int m) {
	int r = x%m;
	return r<0 ? r + m : r;
}