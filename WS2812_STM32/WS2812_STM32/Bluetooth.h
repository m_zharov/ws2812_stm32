#pragma once

// Includes
#include <misc.h>
#include <stm32f4xx_gpio.h>		// Include GPIO header
#include <stm32f4xx_rcc.h>		// Include Real-time Clock Control header
#include <stm32f4xx_usart.h>	// Include USART header
#include <string.h>
#include <stdio.h>
#include <CircularBufferChar.h>
#include <ctype.h>
#include <stdlib.h>
#include <Settings.h>
//#define DEBUG

// Functions
bool BT_initUSART2(uint32_t uiBaudRate, uint16_t uiMode);	// Initialise AF pins, function and interrupt for USART

// BT Global Vars
extern CircularBufferChar BT_TXBuffer;
extern char cNewLine[3];
extern char cCommandAccepted[20];
extern uint8_t cCommand[4];


/**
* \class BT
*
* \brief Robot Bluetooth comms class.
*
* This class controls the command and diagnostics through the BT USART interface.
* Commands will be sent via a string beginning with $ and ending with \r\n
*
* \author Michael Jenkins
* \date 11/10/2015
* \todo Change function to extend a base USART class, merge with GPS
*/
class BT
{
public:
	/** \Brief IMU Constructor.
	*  Sets initial state (if required- currently not)
	*/
	BT();

	/** \Brief BT initialisation function.
	* Sets up USART hardware and other required components for object operation.
	* \note Includes setup of circular buffer
	*/
	void initialise();

	/** \Brief BT USART Recieve Function
	* Recieves bytes from USART and appends them to current recieve string. Upon receipt of \r\n (CRLF) this function will break the data in to individual components and clear recieve string.
	*/
	void BT_Recieve(char byte);

	/** \Brief BT USART Send Function
	* Validates data string and (if ok) adds to BT circular buffer which takes care of the rest
	*/
	void BT_Send(const char * string, uint16_t length);

	/** \Brief BT String Parser
	* This function parses the recieved BT string and applies the newly acquired settings to the related object / struct
	*/
	void ParseString();

	/** \Brief BT TX Circular Buffer
	* Buffer that is used to send data or instructions to BT device.
	* Stores TX data after it has been validated by the BT_Send function.
	*/
	CircularBufferChar TXBuffer;

	// Notification strings (available to all)
	static const char errorCRC[];
	static const char errorUnrecognised[];
	static const char noticeProjectInitComplete[];
	static const char errorManualMode[];
protected:
	void initUSARTPins();
	void initUSART(uint32_t uiBaudRate, uint16_t uiMode);

	// Variables //
	///////////////
	char BTString[192];	/*!< Recieve String for BT Control Messages */
	bool isBTStringNew;	/*!< Flag to ensure each newly recieved message is only processed once */

	/** \Brief BT RX Buffer
	* Struct containing a char array, counter and add function
	* Stores incoming RX data until CRLF is recieved and entire string can be interpreted.
	* \todo ensure currentChar is initialised to 0 on boot.
	*/
	struct RXBuffer {
		/** \Brief RXBuffer current location
		* integer pointing to next free slot, incremented after each char is added.
		*/
		uint8_t currentChar;

		/** \Brief CRC Value
		* 1 means first number in CRC, 2 is second. Handled by add().
		*/
		uint8_t currentCRC;

		/** \Brief RXBuffer data array
		* char array for all recieved data from BT module.
		*/
		char data[192];

		/** \Brief Recieved CRC
		* Char to recieve CRC value after the end of a string
		*/
		char crcRx;

		/** \Brief CRC Storage
		* Variable initialised to 0 and XOR'd with each incoming byte
		*/
		char crcLocal;


		/** \Brief add char function
		* add function to ensure all new chars are correctly added and no overflow occurs.
		* \param[in] newChar new char to be added to data array
		* \return boolean to incidate success or failure. function will return false in the event the buffer is full.
		*/
		bool add(char newChar); // Add function, will control adding new chars

		/** \Brief String Verification Function
		* Check for \r\n as the most recently added two chars
		* \return boolean to incidate success or failure. function will return false in the event the two newest chars aren't \r\n..
		*/
		bool checkComplete();

		/** \Brief "Move" string to new location in preparation for processing
		* Copy data to new char array in preparation for data processing, also reset pointer variable to allow new data to be recieved.
		*/
		void move(char * BTString);
	} RXBuffer;
};