#pragma once 

#include <stdint.h>
/**
* \struct Settings
*
* \brief Core Settings Struct
*
* This struct contains the core settings for the system to be used through all classes that require such information.
* Data validation functions are implemented to ensure that safe ranges are obeyed in robot operation.
*
* \todo move core variables in to this function and implement control in other classes e.g. geoFenceDiameter
*
* \author Michael Jenkins
* \date 10/10/2015
*/
struct Settings {
public:
	uint32_t LEDMode;
	bool calculationReady;
	bool staticBright;
	uint8_t intensity; // LED brightness (0 to 255)
	uint8_t colour_g;	// LED Colour Green
	uint8_t colour_r;   // LED Colour Red
	uint8_t colour_b;   // LED Colour Blue
	uint8_t counter;	// counter updated every time updatemillis is reached
	uint32_t HSVStepSize;
	double BrightnessStepSize;
	
	uint8_t getUpdateMillis()
	{
		return updateMillis;
	}

	void setUpdateMillis(uint8_t newUpdateMillis)
	{
		if (newUpdateMillis > 200)
		{
			updateMillis = 300;
		}
		else if (newUpdateMillis < 5)
		{
			updateMillis = 5;
		}
		else
		{
			updateMillis = newUpdateMillis;
		}
	}
protected:
	uint8_t updateMillis; /*!< Count value to trigger update functions in core program */
};

extern Settings settings;