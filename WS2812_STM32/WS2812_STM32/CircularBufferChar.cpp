#include "CircularBufferChar.h"
// Constructor using initialiser list... C++11 to give slight efficiency boost to chunky objects

// New CircularBufferChar object, canOverwrite allows excess data to overwrite oldest data
CircularBufferChar::CircularBufferChar(bool canOverwrite) : canOverwrite(canOverwrite),
head(0),	// Start buffer at location 0
tail(0),	// Using a count so 1 slot doesn't have to remain free
count(0)	// Buffer starts empty
{
	// Yay, nothing, thanks C++11... or something
}

// Placeholder until I can find a better way: allow fn to disable TX interrupts
// - Possible options: lambda function, ???
void CircularBufferChar::Initialise(uint32_t uiPeripheral)
{
	this->uiPeripheral = uiPeripheral;
}

// Empty the buffer by resetting indexes
void CircularBufferChar::Empty()
{
	head = 0;
	tail = 0;
	count = 0;
}

// Push Back
// - TODO: Allow Overwriting
// - TODO: Overload to allow single char
bool CircularBufferChar::PushBack(const char * item, uint16_t length)
{
	// Ensure that too much data isn't being added
	if ((length - 1) > BUFFER_SIZE)
		return false;

	// Ensure overwrite is off and buffer isn't full
	if (!canOverwrite && (count == BUFFER_SIZE))
		return false;

	// Set up vars for check if there is enough room and copy data in
	uint16_t headLength = head + length;
	uint16_t remainder = headLength % BUFFER_SIZE;

	// Ensure there is enough room for the new item
	if ((headLength - tail) > BUFFER_SIZE)
		return false;

	// Copy new data in (in two chunks if necessary)
	if (headLength > BUFFER_SIZE)
	{
		memcpy(&buffer[head], item, length - remainder);
		memcpy(&buffer[0], &item[length - remainder], remainder);
	}
	else
	{
		memcpy(&buffer[head], item, length);
	}

	// Update details
	uint16_t oldCount = count;
	count += length;
	head = (head + length) % BUFFER_SIZE;

	// Do item specific things
	switch (uiPeripheral)
	{
	case USART6_BASE:
		if (oldCount == 0 && count > 0)	// Enable interrupts if old cound was 0 and new is larger
			USART_ITConfig(USART6, USART_IT_TXE, ENABLE);
		break;
	case USART2_BASE:
		if (oldCount == 0 && count > 0)	// Enable interrupts if old cound was 0 and new is larger
			USART_ITConfig(USART2, USART_IT_TXE, ENABLE);
		break;
	case USART1_BASE:
		if (oldCount == 0 && count > 0)	// Enable interrupts if old cound was 0 and new is larger
			USART_ITConfig(USART1, USART_IT_TXE, ENABLE);
		break;
	default:
		break;
	}

	return true;
}

// Get Oldest
// - Sets input to '\0' on error
// - Returns oldest char and advances the tail
void CircularBufferChar::GetOldest(char * item)
{
	// Ensure that there is something in the buffer to read
	if (count == 0)
	{
		*item = '\0';
		return;
	}

	// Get single item out
	*item = buffer[tail];

	// Increment tail and loop if required
	tail = (tail + 1) % BUFFER_SIZE;
	// decrement count
	--count;

	// Do item specific things
	switch (uiPeripheral)
	{
	case USART6_BASE:
		if (count == 0)
			USART_ITConfig(USART6, USART_IT_TXE, DISABLE);
		break;
	case USART2_BASE:
		if (count == 0)
			USART_ITConfig(USART2, USART_IT_TXE, DISABLE);
		break;
	case USART1_BASE:
		if (count == 0)
			USART_ITConfig(USART1, USART_IT_TXE, DISABLE);
		break;
	default:
		break;
	}
}

// Get Newest
// - Sets input to '\0' on error
// - Returns oldest char and advances the tail
void CircularBufferChar::GetNewest(char * item)
{
	// Ensure that there is something in the buffer to read
	if (count == 0)
	{
		*item = '\0';
		return;
	}

	// Get single item out
	*item = buffer[head - 1];
}

// Returns the current number of items in the buffer
uint16_t CircularBufferChar::GetCount()
{
	return count;
}

bool CircularBufferChar::IsEmpty()
{
	return (count == 0);
}

bool CircularBufferChar::IsFull()
{
	return (count == BUFFER_SIZE);
}