#include "Bluetooth.h"

BT::BT() : BTString(""), isBTStringNew(false)
{
}

void BT::initialise()
{
	// Set up RXBuffer
	RXBuffer.currentChar = 0;

	// Initialise USART Pins
	initUSARTPins();

	// Initialise USART
	initUSART(9600, USART_Mode_Rx | USART_Mode_Tx);

	// Configure Circular Buffer
	TXBuffer.Initialise(USART1_BASE);
}

void BT::BT_Recieve(char byte)
{
	// Loopback for user interactivity
//	TXBuffer.PushBack(&byte, 1); //!!!!!!

	// Add byte to buffer
	if (!RXBuffer.add(byte))
	{
		// Oh... the buffer is full... this is a problem
		// (or the first char is invalid)
		return;
	}

//	if (strcmp(RXBuffer.data, "OKsetPIN") == 0)
//	{
//		int fff = 1;
//	}
//	if (strcmp(RXBuffer.data, "OKsetname") == 0)
//	{
//		int fff = 1;
//	}

	// Check that string is complete
	if (RXBuffer.checkComplete())
	{
#ifdef DEBUG
		printf("BT::BT_Recieve -> String Recieved: %s\r\n", RXBuffer.data);
#endif

		// Set flag
		isBTStringNew = true;

		// Copy Complete String
		RXBuffer.move(BTString);

		BT_Send("\r\n", 2);
	}


}

void BT::BT_Send(const char * string, uint16_t length)
{
	// Verify String (check for \r\n at end) - keep the terminal clean
//	if (string[length - 2] != '\r' || string[length - 1] != '\n')
//	{
//		return;
//	}

	// Add string to TX buffer
	TXBuffer.PushBack(string, length);
}

void BT::ParseString()
{
	// Check that the string is new
	if (!isBTStringNew)
	{
		return;
	}

	char lStr[192];		// New buffer
	char * strPtr;		// Pointer for strtok
	char retStr[50];	// String to return
	int8_t BTSubject = 0; // Variable to change
	int8_t BTtype = -1; // 0 = read, 1 = write, 2 = increment, 3 = decrement
	uint32_t BTValue1 = 0; // new value, 0 to 255
	uint32_t BTValue2 = 0; // new value, 0 to 255
	uint32_t BTValue3 = 0; // new value, 0 to 255
	bool isParsedStringValid = false;
	retStr[0] = '\0';

	// Copy current string in to working string
	strcpy(lStr, BTString);

	// String has been copied and is no longer 'new' to this function
	isBTStringNew = false;

	// Break string apart by tokens
	strPtr = strtok(lStr, "$,.*");

	// Check for BT string type...
	if ((strPtr != NULL) && (strcmp("BT", strPtr) == 0)) // ALL bt strings must start with $BT,
	{
		strPtr = strtok(NULL, "$,.*");
		if (strPtr == NULL)
		{
			return;
		}
		BTSubject = atoi(strPtr);	// Command Subject

		strPtr = strtok(NULL, "$,.*");
		if (strPtr == NULL)
		{
			return;
		}
		BTtype = atoi(strPtr);	// Command Type

		isParsedStringValid = true;

		strPtr = strtok(NULL, "$,.*");
		if (strPtr != NULL)
		{
			BTValue1 = atoi(strPtr);	// New Value
			
			strPtr = strtok(NULL, "$,.*");
			if (strPtr != NULL)
			{
				BTValue2 = atoi(strPtr);	// New Value
				
				strPtr = strtok(NULL, "$,.*");
				if (strPtr != NULL)
				{
					BTValue3 = atoi(strPtr);	// New Value
				}
			}
		}
	}

	// Do actions if possible
	if (isParsedStringValid)
	{
		switch (BTSubject)
		{
		case 1:
			settings.setUpdateMillis(BTValue1);
			sprintf(retStr, "Update Rate Changed to %d!\r\n", settings.getUpdateMillis());
			break;
		case 2:
			settings.intensity = BTValue1;
			sprintf(retStr, "Intensity Changed to %d!\r\n", settings.intensity);
			break;
		case 3:
			settings.colour_r = BTValue1;
			settings.colour_g = BTValue2;
			settings.colour_b = BTValue3;
			sprintf(retStr, "Colour Changed to \"%d,%d,%d\"!\r\n", settings.colour_r, settings.colour_g, settings.colour_b);
			break;
		case 4:
			settings.LEDMode = (settings.LEDMode + 1) % 4 ;
			sprintf(retStr, "LED Mode = %d!\r\n", settings.LEDMode);
			break;
		case 5:
			settings.staticBright = !settings.staticBright;
			sprintf(retStr, "Static Brightness = %d!\r\n", settings.staticBright);
			break;
		case 6:
			settings.HSVStepSize = BTValue1;
			sprintf(retStr, "HSV Step Size = %d!\r\n", settings.HSVStepSize);
			break;
		case 7:
			settings.BrightnessStepSize = (double)BTValue1 / 1000.0;
			sprintf(retStr, "Brightness Step Size = %d / 1000!\r\n", BTValue1);
			break;
		default:
			sprintf(retStr, "Unrecognised Command Subject!\r\n", settings.staticBright);
			break;
		}
	}
	else
	{
		sprintf(retStr, errorUnrecognised);
	}

	// Return Confirmation
	BT_Send(retStr, strlen(retStr));
}

void BT::initUSARTPins()
{
	// USART Setup Information
	// Set up PINs that will be used first
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_USART1);	// Define specific AF on pin 6 (USART1 - TX)
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource7, GPIO_AF_USART1);	// Define specific AF on pin 7 (USART1 - RX)
	GPIO_InitTypeDef GPIO_InitStruct;
	// Initialize pins as alternate function
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;			// Pick which pin(s) to init - Pin 6/7
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;					// Set pin(s) mode to AF
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;					// Set output type to Push/Pull (Driven hard / cleaner edge)
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;					// Set internal resistor to pull up
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;				// 50MHz pin clock, easily fast enough for 9600bps
	GPIO_Init(GPIOB, &GPIO_InitStruct);							// Set data on selected port using struct
}

void BT::initUSART(uint32_t uiBaudRate, uint16_t uiMode)
{
	// USART Setup Information
	// Set up USART Peripheral Device - 9600 8 N 1
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);			// Give the USART a clock
	USART_InitTypeDef USART_InitStruct;
	USART_InitStruct.USART_BaudRate = uiBaudRate;					// Set Baud Rate to 9600bps
	USART_InitStruct.USART_WordLength = USART_WordLength_8b; 		// Set Word Length to 8
	USART_InitStruct.USART_Parity = USART_Parity_No;				// Set Parity to N
	USART_InitStruct.USART_StopBits = USART_StopBits_1;				// Set Stop Bits to 1
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None; // Disable hardware flow control
	USART_InitStruct.USART_Mode = uiMode;							// Enable both Tx and Rx
	USART_Init(USART1, &USART_InitStruct);							// Send data to USART

	// Set up interrupt controller for usart
	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel = USART1_IRQn;					// Set IRQ channel to USART1
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;					// Enable Interrupts for USART1
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;			// Set USART to highest priority
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;					// Set USART to highest sub priority
	NVIC_Init(&NVIC_InitStruct);

	// Clear any current interrupt flags
	USART_ClearITPendingBit(USART1, USART_IT_RXNE);	// Clear USART 1 RX interrupt bit
	USART_ClearITPendingBit(USART1, USART_IT_TXE);	// Clear USART 1 TX interrupt bit

	// Enable USART Module / RX and TX interrupts
	USART_Cmd(USART1, ENABLE);						// Enable USART Module
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);	// Enable USART1 RX Interrupt
	USART_ITConfig(USART1, USART_IT_TXE, DISABLE);	// Disable USART1 TX Interrupt (Enable when there is something to send)
}

bool BT::RXBuffer::add(char newChar)
{
	if (newChar == '$')
	{
		currentChar = 0;
	}
	if (currentChar == 0 && newChar != '$')
	{
		return false;
	}

	// Check for overflow, if string is too long start again
	if (currentChar > 195)
	{
		currentChar = 0;
		currentCRC = 0;
		crcLocal = 0;
		return false;
	}

	if (currentCRC > 0)
	{
		if (currentCRC == 1)
		{
			crcRx = (isalpha(newChar) ? (toupper(newChar) - 55) : (newChar - '0')) << 4;
		}
		else if (currentCRC == 2)
		{
			crcRx += (isalpha(newChar) ? (toupper(newChar) - 55) : (newChar - '0'));
		}
		++currentCRC;
	}
	else
	{
		if (newChar == '*') // Check for end of string
		{
			// Start Recieving CRC
			//currentCRC = 1;
		}
		else if (currentChar > 0) // Not end of string, add new char
		{
			// Update checksum with every char after the first $ until *
			crcLocal ^= newChar;
		}
	}

	// Add new char in to array
	data[currentChar++] = newChar;

	return true;

}

bool BT::RXBuffer::checkComplete()
{
	if (currentChar > 2)
	{
		if (data[currentChar - 1] == '*') // Don't need CRC (use -3 if its required)
		{
			return true;
		}
	}
	return false;
}

void BT::RXBuffer::move(char * GPSString)
{
	data[currentChar] = '\0';
	strncpy(GPSString, data, currentChar);
	currentChar = 0;
	currentCRC = 0;
	crcLocal = 0;
	crcRx = 0;
}

// Useful Strings - Static
const char BT::errorCRC[] = "Invalid Command: CRC Error.\r\n";
const char BT::errorUnrecognised[] = "Invalid Command: Unrecognised.\r\n";
const char BT::noticeProjectInitComplete[] = "RoboKart initialisation complete.\r\n";
const char BT::errorManualMode[] = "Invalid Command: Kart is in Manual Mode.\r\n";